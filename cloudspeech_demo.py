#!/usr/bin/env python3
# Copyright 2017 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""A demo of the Google CloudSpeech recognizer."""

import aiy.audio
import aiy.cloudspeech
import aiy.voicehat
import aiy._drivers._led

def main():
    recognizer = aiy.cloudspeech.get_recognizer()
    recognizer.expect_phrase('turn off the light')
    recognizer.expect_phrase('turn on the light')
    recognizer.expect_phrase('blink')

    button = aiy.voicehat.get_button()
    led = aiy.voicehat.get_led()
    aiy.audio.get_recorder().start()
    
    first_led = aiy._drivers._led.LED(channel=16)
    second_led = aiy._drivers._led.LED(channel=20)
    third_led = aiy._drivers._led.LED(channel=21)
    forth_led = aiy._drivers._led.LED(channel=25)

    first_led.start()
    second_led.start()
    third_led.start()
    forth_led.start()

    while True:
        print('Press the button and speak')
        button.wait_for_press()
        print('Listening...')
        text = recognizer.recognize()
        if text is None:
            print('Sorry, I did not hear you.')
        else:
            print('You said "', text, '"')
            if 'turn on the light' in text:
                first_led.set_state(aiy.voicehat.LED.ON)
                second_led.set_state(aiy.voicehat.LED.ON)
                third_led.set_state(aiy.voicehat.LED.ON)
                forth_led.set_state(aiy.voicehat.LED.ON)
            elif 'turn off the light' in text:
                first_led.set_state(aiy.voicehat.LED.OFF)
                second_led.set_state(aiy.voicehat.LED.OFF)
                third_led.set_state(aiy.voicehat.LED.OFF)
                forth_led.set_state(aiy.voicehat.LED.OFF)
            elif 'blink' in text:
                first_led.set_state(aiy.voicehat.LED.BLINK)
                second_led.set_state(aiy.voicehat.LED.BLINK)
                third_led.set_state(aiy.voicehat.LED.BLINK)
                forth_led.set_state(aiy.voicehat.LED.BLINK)
            elif 'goodbye' in text:
                break


if __name__ == '__main__':
    main()
